<?php
// $Id: panels_views_prerender.admin.inc,v 1.1.6.1 2011-04-11 11:08:32 jln Exp $

/**
 * @file
 * 
 * Settings for panels_views_prerender
 */

/**
 *
 */
function panels_views_prerender_settings_form() {
  $form = array();
  
  // General settings
  
  $form['general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['general_settings']['panels_views_prerender_update_on_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Update on each cron run?'),
    '#default_value' => variable_get('panels_views_prerender_update_on_cron', TRUE),
    '#description' => t('You can always call %path to re-render the panes', array('%path' => 'cron/panels_views_prerender/render')),
  );
  
  $run_interval_options = array();
  for ($i = 60; $i < 3600; $i+= 60) {
    $run_interval_options[$i] = t('!minutes minute(s)', array('!minutes' => ($i / 60)));
  }
  $form['general_settings']['panels_views_prerender_minimum_update_interval'] = array(
    '#type' => 'select',
    '#title' => t('Update interval'),
    '#options' => $run_interval_options,
    '#default_value' => variable_get('panels_views_prerender_minimum_update_interval', 120),
    '#description' => t('Select the life time for panes.'),
  );
  
  $timeout_options = array();
  for ($i = 600; $i < 4200; $i+= 600) {
    $timeout_options[$i] = t('!minutes minutes', array('!minutes' => ($i / 60)));
  }
  $form['general_settings']['panels_views_prerender_update_timeout'] = array(
    '#type' => 'select',
    '#title' => t('Update timeout'),
    '#options' => $timeout_options,
    '#default_value' => variable_get('panels_views_prerender_update_timeout', 3600),
    '#description' => t('If an update of the prerendered views has run for more than selected minutes, it has properbly crashed so we allow it to run again next time.'),
  );
  
  // Filter settings
  
  $form['displays_to_filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Displays to show/hide'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['displays_to_filter']['panels_views_prerender_views_include_selected_panes_and_displays'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include the selected options in content selection?'),
    '#default_value' => variable_get('panels_views_prerender_views_include_selected_panes_and_displays', TRUE),
    '#description' => t('If checked, only the selected displays/panes will be shown on the select displays. If unchecked, it will blacklist the selected displays/panes.<br />If none is selected, no filtering will be performed.'),
  );
  
  ctools_include('content');
  
  ctools_get_content_type('views_panes');
  $views_panes = views_content_views_panes_content_type_content_types(array());
  $views_panes_options = array();
  foreach ($views_panes as $name => $data) {
    $views_panes_options[$name] = $name;
  }
  $form['displays_to_filter']['panels_views_prerender_views_panes_to_filter'] = array(
    '#type' => 'select',
    '#title' => t('Views panes'),
    '#options' => $views_panes_options,
    '#multiple' => TRUE,
    '#default_value' => variable_get('panels_views_prerender_views_panes_to_filter', array()),
  );
  
  ctools_get_content_type('views');
  $views_displays = views_content_views_content_type_content_types(array());
  $views_displays_options = array();
  foreach ($views_displays as $name => $data) {
    $views_displays_options[$name] = $name;
  }
  $form['displays_to_filter']['panels_views_prerender_views_displays_to_filter'] = array(
    '#type' => 'select',
    '#title' => t('Views displays'),
    '#options' => $views_displays_options,
    '#multiple' => TRUE,
    '#default_value' => variable_get('panels_views_prerender_views_displays_to_filter', array()),
  );
  
  return system_settings_form($form);
}
