<?php
// $Id: views_panes_prerender.inc,v 1.1.6.1 2011-04-11 11:08:32 jln Exp $


/**
 * @file
 * 
 * Much the same as the content_type views_panes.inc, but this lets us rely on stored data.
 * It utilizes most of the functions from views_panes.inc from ctools
 */

/**
 * Implementation of hook_ctools_content_types()
 */
function panels_views_prerender_views_panes_prerender_ctools_content_types() {
  // Let content type views generate some cache stuff before we allow this module to hijack its functions.
  ctools_get_content_type('views_panes');
  return array(
    'title' => t('View panes pre-rendered'),
    'admin settings' => 'views_content_admin_form',
    'js' => array(drupal_get_path('module', 'ctools') . '/js/dependent.js'),
  );
}

/**
 * Return all content types available.
 */
function panels_views_prerender_views_panes_prerender_content_type_content_types($plugin) {
  $types = views_content_views_panes_content_type_content_types($plugin);

  // Filter on the displays hidden/shown on the settings page
  $filter_displays = variable_get('panels_views_prerender_views_panes_to_filter', array());
  if (!empty($filter_displays)) {
    $include_selected = variable_get('panels_views_prerender_views_include_selected_panes_and_displays', TRUE);
    if (!empty($include_selected)) {
      $types = array_intersect_key($types, $filter_displays);
    }
    else {
      $types = array_diff_key($types, $filter_displays);
    }
  }

  foreach ($types as &$type) {
    $type['category'] = t('Views panes pre-rendered');
    $type['icon'] = 'icon_views_prerender.png';
  }
  return $types;
}

/**
 * Return a single content type.
 */
function panels_views_prerender_views_panes_prerender_content_type_content_type($subtype, $plugin) {
  return views_content_views_panes_content_type_content_type($subtype, $plugin);
}


/**
 * Output function for the 'views' content type.
 *
 * Outputs a view based on the module and delta supplied in the configuration.
 */
function panels_views_prerender_views_panes_prerender_content_type_render($subtype, $conf, $panel_args, $contexts, $dummy, $use_cache = TRUE) {
  // Do cache stuff here!
  if ($use_cache) {
    $block = _panels_views_prerender_get_rendered_content($subtype, $conf, $panel_args, $contexts);
    if (!empty($block)) {
      return $block;
    }
    elseif (is_null($block)) {
      // We found a pre-rendered block with no content, but since it was not FALSE, it simply doesn't have any content...
      return '';
    }
  }
  
  // Fall back to dynamic view
  $block = views_content_views_panes_content_type_render($subtype, $conf, $panel_args, $contexts);
  
  // Store it!
  _panels_views_prerender_set_rendered_content($subtype, $conf, $panel_args, $contexts, $block);
  
  return $block;
}

/**
 * Returns an edit form for a block.
 */
function panels_views_prerender_views_panes_prerender_content_type_edit_form(&$form, &$form_state) {
  views_content_views_panes_content_type_edit_form($form, $form_state);
  if (!empty($form_state['contexts'])) {
    drupal_set_message(t('Pre-rendered panes does not support dynamic arguments and contexts.'), 'error');
  }
}

/**
 * Store form values in $conf.
 */
function panels_views_prerender_views_panes_prerender_content_type_edit_form_submit(&$form, &$form_state) {
  views_content_views_panes_content_type_edit_form_submit($form, $form_state);
}

/**
 * Returns the administrative title for a type.
 */
function panels_views_prerender_views_panes_prerender_content_type_admin_title($subtype, $conf) {
  ctools_get_content_type('views_panes');
  
  list($name, $display) = explode('-', $subtype);
  $view = views_get_view($name);
  if (empty($view) || empty($view->display[$display])) {
    return t('Deleted/missing view @view', array('@view' => $name));
  }

  $view->set_display($display);
  $title = $view->display_handler->get_option('pane_title');
  if (empty($title)) {
    $title = $view->display[$view->current_display]->display_title;
  }
  return t('(PR) View pane: @name', array('@name' => $view->name . '-' . $title));
}

/**
 * Returns the administrative title for a type.
 */
function panels_views_prerender_views_panes_prerender_content_type_admin_info($subtype, $conf, $contexts) {
  ctools_get_content_type('views_panes');
  
  $block = views_content_views_panes_content_type_admin_info($subtype, $conf, $contexts);
  $block->title = t('Pre-rendered views pane') . '<br />' . $block->title;
  return $block;
}
