<?php
// $Id: views_prerender.inc,v 1.1.6.1 2011-04-11 11:08:32 jln Exp $


/**
 * @file
 * 
 * Much the same as the content_type views.inc, but this lets us rely on stored data.
 * It utilizes most of the functions from views.inc from ctools
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
if (variable_get('ctools_content_all_views', TRUE)) {
  // Let content type views generate some cache stuff before we allow this module to hijack its functions.
  ctools_get_content_type('views');
  
  $plugin = array(
    'title' => t('All views pre-rendered'),
    'defaults' => array(
      'override_pager_settings' => FALSE,
      'use_pager' => FALSE,
      'nodes_per_page' => 10,
      'pager_id' => 0,
      'offset' => 0,
      'more_link' => FALSE,
      'feed_icons' => FALSE,
      'panel_args' => FALSE,
      'link_to_view' => FALSE,
      'args' => '',
      'url' => '',
    ),
    'add form' => array(
      'views_content_views_select_display' => t('Select display'),
      'panels_views_prerender_views_prerender_content_type_edit_form' => array(
        'default' => TRUE, // put wrapper here, not on the previous form.
        'title' => t('Configure view'),
      ),
    ),
    'all contexts' => FALSE,
  );
}

/**
 * Return all content types available.
 */
function panels_views_prerender_views_prerender_content_type_content_types($plugin) {
  $types = views_content_views_content_type_content_types($plugin);

  // Filter on the displays hidden/shown on the settings page
  $filter_displays = variable_get('panels_views_prerender_views_displays_to_filter', array());
  if (!empty($filter_displays)) {
    $include_selected = variable_get('panels_views_prerender_views_include_selected_panes_and_displays', TRUE);
    if (!empty($include_selected)) {
      $types = array_intersect_key($types, $filter_displays);
    }
    else {
      $types = array_diff_key($types, $filter_displays);
    }
  }

  foreach ($types as &$type) {
    $type['category'] = t('Views pre-rendered');
    $type['icon'] = 'icon_views_prerender.png';
  }
  return $types;
}

/**
 * Return a single content type.
 */
function panels_views_prerender_views_prerender_content_type_content_type($subtype, $plugin) {
  return views_content_views_content_type_content_type($subtype, $plugin);
}

/**
 * Output function for the 'views' content type.
 *
 * Outputs a view based on the module and delta supplied in the configuration.
 */
function panels_views_prerender_views_prerender_content_type_render($subtype, $conf, $panel_args, $contexts, $dummy, $use_cache = TRUE) {
  // Do cache stuff here!
  if ($use_cache) {
    $block = _panels_views_prerender_get_rendered_content($subtype, $conf, $panel_args, $contexts);
    if (!empty($block)) {
      return $block;
    }
    elseif (is_null($block)) {
      // We found a pre-rendered block with no content, but since it was not FALSE, it simply doesn't have any content...
      return '';
    }
  }
  
  // Fall back to dynamic view
  $block = views_content_views_content_type_render($subtype, $conf, $panel_args, $contexts);
  
  // Store it!
  _panels_views_prerender_set_rendered_content($subtype, $conf, $panel_args, $contexts, $block);
  
  return $block;
}

/**
 * Submit the basic view edit form.
 *
 * This just dumps everything into the $conf array.
 */
function panels_views_prerender_views_select_display_submit(&$form, &$form_state) {
  views_content_views_select_display_submit($form, $form_state);
}

/**
 * Returns an edit form for a block.
 */
function panels_views_prerender_views_prerender_content_type_edit_form(&$form, &$form_state) {
  views_content_views_content_type_edit_form($form, $form_state);
  
  // Disable contexts and tell user, that one can't use contexts with prerendered panes
  if (!empty($form['context'])) {
    foreach ($form['context'] as &$form_context) {
      if (is_array($form_context)) {
        $form_context['#disabled'] = TRUE;
      }
    }
    
    drupal_set_message(t('Pre-rendered panes does not support dynamic contexts.'), 'error');
  }
  
  // Disable panel args and tell user, that one can't use dynamic arguments with prerendered panes
  if (!empty($form['panel_args'])) {
    $form['panel_args']['#disabled'] = TRUE;
    
    drupal_set_message(t('Pre-rendered panes does not support dynamic arguments.'), 'error');
  }
}

/**
 * Store form values in $conf.
 */
function panels_views_prerender_views_prerender_content_type_edit_form_submit(&$form, &$form_state) {
  views_content_views_content_type_edit_form_submit($form, $form_state);
}

/**
 * Returns the administrative title for a type.
 */
function panels_views_prerender_views_prerender_content_type_admin_title($subtype, $conf) {
  ctools_get_content_type('views');
  
  $view = _views_content_views_update_conf($conf, $subtype);

  if (!is_object($view)) {
    return t('Deleted/missing view @view', array('@view' => $view));
  }

  $title = $view->display[$view->current_display]->display_title;
  return t('(PR) view: @name', array('@name' => $view->name . '-' . $title));
}

/**
 * Returns the administrative title for a type.
 */
function panels_views_prerender_views_prerender_content_type_admin_info($subtype, $conf, $contexts) {
  ctools_get_content_type('views');
  
  $block = views_content_views_content_type_admin_info($subtype, $conf, $contexts);
  $block->title = t('Pre-rendered views display') . '<br />' . $block->title;
  return $block;
}
